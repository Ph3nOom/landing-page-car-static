//Count to function

$(document).ready(function() {

$(window).scroll(function () {


        $('.counter').each(function () {



            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();

            if( bottom_of_window > bottom_of_object ) {
                var $this = $(this),
                    countTo = $this.attr('data-count');

                $({countNum: $this.text()}).animate({
                        countNum: countTo
                    },

                    {

                        duration: 2500,
                        easing: 'linear',
                        step: function () {
                            $this.text(Math.floor(this.countNum));
                        },
                        complete: function () {
                            $this.text(this.countNum);
                            //alert('finished');
                        }

                    });
            }

        });


});
});

//Tooltip
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip()
});