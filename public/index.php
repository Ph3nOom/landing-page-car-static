<?php include("partials/header.php") ?>

    <section id="header">
        <div class="background">
            <div class="container">
                <div class="headerContent">
                    <h1>Littus Cars</h1>
                    <hr>
                    <p>A car is not just a transportation method, it´s a statement.</p>
                </div>
                <div class="buttonGroup">
                    <a class="buttonLink page-scroll" href="#about">Our Car Collection</a>
                    <a class="buttonLink page-scroll transparentButton" href="#about">Book a Testdrive</a>
                </div>
            </div>
            <div class="imgBackground"></div>
        </div>
    </section>

    <section id="collection">
        <div class="container collectionContainer">
            <div class="collectionHeading">
                <h1>Our Car Collection</h1>
                <hr>
                <p class="faded">For every taste and occasion.</p>
            </div>
            <div class="collectionCars">
                <div class="row top">
                    <div class="col-lg-4 col-xs-12 zoomIn">
                        <div class="noOverflow">
                            <img src="img/rebecca.jpg" alt="Rebbecca">
                        </div>
                        <div class="collectionOverlay">
                            <p class="overlayText">Rebbecca</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-12 zoomIn">
                        <div class="noOverflow">
                            <img src="img/babe.jpg" alt="Babe">
                        </div>
                        <div class="collectionOverlay">
                            <p class="overlayText">Babe</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-12 zoomIn">
                        <div class="noOverflow">
                            <img src="img/margo.jpg" alt="Margo">
                        </div>
                        <div class="collectionOverlay">
                            <p class="overlayText">Margo</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-xs-12 zoomIn">
                        <div class="noOverflow">
                            <img src="img/susan.jpg" alt="Susan">
                        </div>
                        <div class="collectionOverlay">
                            <p class="overlayText">Susan</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-12 zoomIn">
                        <div class="noOverflow">
                            <img src="img/joana.jpg" alt="Joana">
                        </div>
                        <div class="collectionOverlay">
                            <p class="overlayText">Joana</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-12 zoomIn">
                        <div class="noOverflow">
                            <img src="img/shelly.jpg" alt="Shelly">
                        </div>
                        <div class="collectionOverlay">
                            <p class="overlayText">Shelly</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="track">
        <div class="jumbotron trackContainer">

            <div class="container">
                <div class="trackHeading">
                    <h3 class="faded">Our track record for the past year</h3>
                </div>
                <div class="row counterRow">
                    <div class="col-lg-3 col-xs-12">
                        <div class="counter" data-count="365">
                            <h3 class="counterHead">1</h3>
                        </div>
                        <div class="counterText">
                            <p>Happy Car Owners</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <div class="counter" data-count="12">
                            <h3 class="counterHead">1</h3>
                        </div>
                        <div class="counterText">
                            <p>Returned Cars</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <div class="counter" data-count="256">
                            <h3 class="counterHead">1</h3>
                        </div>
                        <div class="counterText">
                            <p>Insured Cars</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <div class="counter" data-count="46">
                            <h3 class="counterHead">1</h3>
                        </div>
                        <div class="counterText">
                            <p>Returning Customers</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="banner">
        <div class="bannerBackground">
            <div class="bannerText">
                <h1>Your car is waiting for you...</h1>
            </div>
            <div class="bannerImg"></div>
        </div>
    </section>

    <section id="testDrive">
        <div class="jumbotron testContainer">
            <div class="container">
                <div class="col-lg-12 col-xs-12">
                    <h2>Call us today to book a test drive for 24 hours, completely free.</h2>
                </div>
                <div class="col-lg-12 col-xs-12 centered">
                    <a class="buttonLink page-scroll highlightButton" href="#about">Book Your Test Drive</a>
                </div>
            </div>
        </div>
    </section>

    <section id="littusTeam">
        <div class="container teamContainer">
            <div class="col-lg-12 col-xs-12">
                <h3>The Team Behind Littus Cars</h3>
            </div>
            <div class="row">
                <div class="col-lg-4 col-xs-12 teamCentered">
                    <img class="teamMember" src="img/jane_mettews.jpg">
                    <h2>Jane Mettews</h2>
                    <span class="role">Manager / Founder</span>
                    <div class="col-lg-12 col-xs-12">
                        <a href="mailto:test" target="_blank" class="fa fa-envelope"></a>
                        <a href="https://instagram.com" target="_blank" class="fa fa-instagram"></a>
                        <a href="https://facebook.com" target="_blank" class="fa fa-facebook"></a>
                        <a href="https://twitter.com" target="_blank" class="fa fa-twitter"></a>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12 teamCentered">
                    <img class="teamMember" src="img/gerard_nord.jpg">
                    <h2>Gerard Nord</h2>
                    <span class="role">Dealer / Founder</span>
                    <div class="col-lg-12 col-xs-12">
                        <a href="mailto:test" target="_blank" class="fa fa-envelope"></a>
                        <a href="https://instagram.com" target="_blank" class="fa fa-instagram"></a>
                        <a href="https://facebook.com" target="_blank" class="fa fa-facebook"></a>
                        <a href="https://twitter.com" target="_blank" class="fa fa-twitter"></a>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12 teamCentered">
                    <img class="teamMember" src="img/pam_filligan.jpg">
                    <h2>Pam Filligan</h2>
                    <span class="role">Engineer / Mechanic</span>
                    <div class="col-lg-12 col-xs-12">
                        <a href="mailto:test" target="_blank" class="fa fa-envelope"></a>
                        <a href="https://instagram.com" target="_blank" class="fa fa-instagram"></a>
                        <a href="https://facebook.com" target="_blank" class="fa fa-facebook"></a>
                        <a href="https://twitter.com" target="_blank" class="fa fa-twitter"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="testimonials2">
        <div class="container">
            <div class="row rowSameHeight">
                <div class="content sameHeight col-lg-6">
                    <div class="image" style="background-image: url('img/edgar_neureman.jpg')">
                    </div>
                    <div class="text">
                        <i>Gerard helped find the car of my dreams, period! Littus Cars is the most professional car
                            dealership I have ever worked with.</i>
                        <br>
                        <b>Edgar Neureman</b>
                    </div>
                </div>
                <div class="content sameHeight col-lg-6">
                    <div class="image" style="background-image: url('img/cpt_seegoll.jpg')">
                    </div>
                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil, id.</div>
                </div>
            </div>
        </div>
    </section>

    <section id="partner">
        <div class="container partnerContainer">
            <!--            <div class="col-lg-12 col-xs-12">-->
            <!--                <h1 class="heading">Our Partners</h1>-->
            <!--            </div>-->
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <a href="https://www.mercedes-benz.de" target="_blank">
                        <img class="brand" src="img/mercedes.png" alt="Mercedes Benz">
                    </a>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <a href="https://www.bmw.de" target="_blank">
                        <img class="brand" src="img/bmw.png" alt="BMW">
                    </a>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <a href="https://www.astonmartin.com" target="_blank">
                        <img class="brand" src="img/aston_martin.png" alt="Aston Martin">
                    </a>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <a href="https://www.porsche.com" target="_blank">
                        <img class="brand" src="img/porsche.png" alt="Porsche">
                    </a>
                </div>
            </div>
        </div>
    </section>

<?php include("partials/footer.php") ?>