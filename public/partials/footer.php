<footer id="footer">
    <div class="container contactContainer">
        <div class="col-lg-12 col-xs-12">
            <ul class="contact">
                <li><a class="contactItem" href="mailto:testdrive@littusscars.com">testdrive@littuscars.com</a></li>
                <li><a class="contactItem" href="tel:+6199990000">+61 9999 0000</a></li>
            </ul>
        </div>
        <div class="col-lg-12 col-xs-12 socialButtons">
            <a href="mailto:test" target="_blank" class="fa fa-envelope" data-toggle="tooltip" data-placement="bottom" title="E Mail us!"></a>
            <a href="https://instagram.com" target="_blank" class="fa fa-instagram" data-toggle="tooltip" data-placement="bottom" title="Follow us on Instagram"></a>
            <a href="https://facebook.com" target="_blank" class="fa fa-facebook" data-toggle="tooltip" data-placement="bottom" title="Follow us on Facebook"></a>
            <a href="https://twitter.com" target="_blank" class="fa fa-twitter" data-toggle="tooltip" data-placement="bottom" title="Follow us on Twitter"></a>
        </div>
    </div>
</footer>

</body>
</html>