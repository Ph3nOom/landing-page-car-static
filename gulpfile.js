/********************************
 * Contents                     *
 ********************************/

/**
 * Contents...................You're reading it
 * Imports....................All required JS modules
 * Custom Reporter............Reporter for csslint
 * Paths......................All relevant paths for the gulp process
 * Vendor Paths...............All relevant paths of the vendor files
 * Main Tasks.................The bundled Gulp Tasks
 */


/********************************
 * Imports                      *
 ********************************/

var fs = require('fs');
var gulp = require('gulp');
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var clean = require('gulp-clean');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var plumber = require('gulp-plumber');
var gulpSync = require('gulp-sync')(gulp);
var path = require('path');


/********************************
 * Paths                        *
 ********************************/

var paths = {
    public: 'public/**',
    src: 'public/src/**',
    dist: 'public/dist',
    fonts: 'public/fonts',
    scss_src: 'public/src/scss/**/**',
    scss_dist: 'public/dist/css',
    scss_main: 'public/src/scss/main.scss',
    js_src: 'public/src/js',
    js_dist: 'public/dist/js',
    js_custom: ['public/src/js/classes/**', 'public/src/js/main.js'],
    js_build: ['public/src/js/_vendor.js', 'public/src/js/classes/**', 'public/src/js/main.js']
};


/********************************
 * Vendor Paths (js + fonts)    *
 ********************************/

var vendor_js = [
    "./public/src/node_modules/jquery/dist/jquery.js",
    "./public/src/js/classes/hammer.min.js",
    "./public/src/node_modules/eonasdan-bootstrap-datetimepicker-npm/node_modules/moment/min/moment-with-locales.min.js",
    "./public/src/node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js",
    "./public/src/node_modules/bootstrap-select/dist/js/bootstrap-select.js",
    "./public/src/node_modules/eonasdan-bootstrap-datetimepicker-npm/src/js/bootstrap-datetimepicker.js",
    "./public/src/node_modules/parsleyjs/dist/parsley.min.js",
    "./public/src/node_modules/swiper/dist/js/swiper.jquery.umd.min.js",
    "./public/src/node_modules/swiper/dist/js/swiper.jquery.min.js",
    "./public/src/node_modules/swiper/dist/js/swiper.min.js"
];

var vendor_fonts = [
    "./public/src/node_modules/bootstrap-sass/assets/fonts/*",
    "./public/src/node_modules/font-awesome/fonts/*"
];


/********************************
 * Main Tasks                   *
 ********************************/

gulp.task('vendor', ['vendorJs', 'vendorFonts']);
gulp.task('js', ['jsBundle']);
gulp.task('compile', ['scss', 'js']);
gulp.task('initServe', ['serve', 'watch']);

gulp.task('default', gulpSync.sync(['clean', 'vendor', 'compile', 'watch']));

/********************************
 * Gulp Tasks                   *
 ********************************/

// Clean task:
// Cleans the dist folder
gulp.task('clean', function () {
    return gulp.src(paths.dist)
        .pipe(clean());
});


// Scss task:
// Compiles all scss files to a minified dist version
gulp.task('scss', function () {
    return gulp.src(paths.scss_main)
        .pipe(plumber(function (error) {
            // Output an error message
            gutil.log(gutil.colors.red('Error (' + error.plugin + '): ' + error.message));
            // emit the end event, to properly end the task
            this.emit('end');
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 40 versions'],
            cascade: false
        }))
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest(paths.scss_dist))
});


// Vendor JS Task:
// Concats all vendor_js files
gulp.task('vendorJs', function () {
    return gulp.src(vendor_js)
        .pipe(concat('_vendor.js'))
        .pipe(gulp.dest(paths.js_src));
});


// Vendor font task:
// Copies all vendor fonts
gulp.task('vendorFonts', function () {
    return gulp.src(vendor_fonts)
        .pipe(gulp.dest(paths.fonts));
});


// JS bundle task:
// Bundles vendor_js and your js files
gulp.task('jsBundle', function () {
    return gulp.src(paths.js_build)
        .pipe(plumber(function (error) {
            gutil.log(gutil.colors.red('Error (' + error.plugin + '): ' + error.message));
            this.emit('end');
        }))
        .pipe(concat('all.js'))
        .pipe(gulp.dest(paths.js_dist));
});


// Serve task:
// Serves the page on localhost via BrowserSync
gulp.task('serve', function () {
    browserSync.init({
        proxy: 'localhost/'
        + __dirname.substr(__dirname.lastIndexOf(path.sep) + 1)
        + '/public',
        port: 3000
    });
});


// BrowserSync reload task:
// Reloads the browser
gulp.task('browserSyncReload', function () {
    browserSync.reload();
});


// Watch task:
// Defines all the watched files and the necessary tasks
gulp.task('watch', function () {
    gulp.watch(paths.public, ['browserSyncReload']);
    gulp.watch(paths.scss_src, gulpSync.sync(['scss', 'browserSyncReload']));
    gulp.watch(paths.js_custom, gulpSync.sync(['js', 'browserSyncReload']));
});

